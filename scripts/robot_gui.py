#!/usr/bin/env python3

import sys
import rospy
import moveit_commander
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QLabel, QSlider, QPushButton, QHBoxLayout, QLineEdit, QFrame, QSizePolicy
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QIcon
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
from geometry_msgs.msg import PoseStamped


class RobotGUI(QMainWindow):
    def __init__(self):
        super(RobotGUI, self).__init__()
        self.initUI()
        rospy.init_node('robot_gui', anonymous=True)
        self.joint_pub = rospy.Publisher('/joint_group_position_controller/command', JointState, queue_size=10)
        
        # Subscribing to FK_position 
        rospy.Subscriber('/FK_position', PoseStamped, self.update_position_labels)
        
        # Initialize MoveIt commander
        moveit_commander.roscpp_initialize(sys.argv)
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group_name = "robot_arm"  # Change to your planning group
        self.move_group = moveit_commander.MoveGroupCommander(self.group_name)
        
        # Joint limits
        self.joint_limits = {
            'joint1': (-240, 240),
            'joint2': (-115, 125),
            'joint3': (0, 156),
            'joint4': (-200, 200),
            'joint5': (-120, 120),
            'joint6': (-360, 360),
        }

    def initUI(self):
        self.setWindowTitle('Robot Arm Control')
        
        # Set background color
        self.setStyleSheet("background-color: white; font: bold 14px; border-width: 2px;")

        # Main layout
        main_layout = QVBoxLayout()
        
        # Layout for position labels
        position_layout = QHBoxLayout()
        
        x_text_label = QLabel('X:')
        self.x_display = QLineEdit('0.0')
        self.x_display.setReadOnly(True)
        y_text_label = QLabel('Y:')
        self.y_display = QLineEdit('0.0')
        self.y_display.setReadOnly(True)
        z_text_label = QLabel('Z:')
        self.z_display = QLineEdit('0.0')
        self.z_display.setReadOnly(True)
        
        position_layout.addWidget(x_text_label)
        position_layout.addWidget(self.x_display)
        position_layout.addWidget(y_text_label)
        position_layout.addWidget(self.y_display)
        position_layout.addWidget(z_text_label)
        position_layout.addWidget(self.z_display)
        
        main_layout.addLayout(position_layout)

        # Horizontal layout for sliders and textboxes
        control_layout = QHBoxLayout()

        # Sliders layout
        sliders_layout = QVBoxLayout()
        # Text boxes layout
        textboxes_layout = QVBoxLayout()
        
        self.sliders = {}
        self.labels = {}
        self.textboxes = {}
        for i in range(1, 7):
            # Slider layout
            slider_layout = QHBoxLayout()
            label = QLabel(f'Joint {i}')
            slider = QSlider(Qt.Horizontal)
            slider.setRange(-180, 180)
            slider.setValue(0)
            slider.valueChanged.connect(lambda value, joint=i: self.update_from_slider(joint))
            value_label = QLabel('0°')
            self.sliders[f'joint{i}'] = slider
            self.labels[f'joint{i}'] = value_label
            
            slider_layout.addWidget(label)
            slider_layout.addWidget(slider)
            slider_layout.addWidget(value_label)
            sliders_layout.addLayout(slider_layout)

            # Textbox layout
            textbox_layout = QHBoxLayout()
            textbox_label = QLabel(f'Joint {i}')
            textbox = QLineEdit()
            textbox.setText('0')
            textbox.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
            textbox.editingFinished.connect(lambda joint=i: self.update_from_textbox(joint))
            self.textboxes[f'joint{i}'] = textbox
            
            textbox_layout.addWidget(textbox_label)
            textbox_layout.addWidget(textbox)
            textboxes_layout.addLayout(textbox_layout)

        # Vertical line separator
        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Sunken)
        
        # Add layouts to control layout
        control_layout.addLayout(sliders_layout)
        control_layout.addWidget(line)
        control_layout.addLayout(textboxes_layout)

        # Add control layout to main layout
        main_layout.addLayout(control_layout)

        # Plan and Execute button
        self.plan_execute_button = QPushButton('Move Robot')
        self.plan_execute_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.plan_execute_button.clicked.connect(self.plan_and_execute)
        self.plan_execute_button.setStyleSheet("background-color: red;")
        
        icon = QIcon('/home/ahmed/Pictures/power.png')
        self.plan_execute_button.setIcon(icon)
        self.plan_execute_button.setIconSize(QSize(50, 50))

        # Center the button at the bottom
        button_layout = QHBoxLayout()
        button_layout.addStretch(2)
        button_layout.addWidget(self.plan_execute_button)
        button_layout.addStretch(2)
        main_layout.addLayout(button_layout)

        # Set layout
        central_widget = QWidget()
        central_widget.setLayout(main_layout)
        self.setCentralWidget(central_widget)
        
    def update_position_labels(self, pose_stamped):
        rospy.loginfo(f"Received FK position: {pose_stamped.pose.position.x}, {pose_stamped.pose.position.y}, {pose_stamped.pose.position.z}")
        self.x_display.setText(f'{pose_stamped.pose.position.x:.2f}')
        self.y_display.setText(f'{pose_stamped.pose.position.y:.2f}')
        self.z_display.setText(f'{pose_stamped.pose.position.z:.2f}')

    def update_from_slider(self, joint):
        slider_value = self.sliders[f'joint{joint}'].value()
        self.textboxes[f'joint{joint}'].blockSignals(True)
        self.textboxes[f'joint{joint}'].setText(str(slider_value))
        self.textboxes[f'joint{joint}'].blockSignals(False)
        self.labels[f'joint{joint}'].setText(f'{slider_value}°')
        self.publish_joint_states()

    def update_from_textbox(self, joint):
        try:
            textbox_value = float(self.textboxes[f'joint{joint}'].text())
        except ValueError:
            textbox_value = 0.0
            self.textboxes[f'joint{joint}'].setText('0')
        # Ensure the value is within bounds
        textbox_value = max(min(textbox_value, self.joint_limits[f'joint{joint}'][1]), self.joint_limits[f'joint{joint}'][0])
        self.textboxes[f'joint{joint}'].blockSignals(True)
        self.textboxes[f'joint{joint}'].setText(str(textbox_value))
        self.textboxes[f'joint{joint}'].blockSignals(False)
        self.sliders[f'joint{joint}'].blockSignals(True)
        self.sliders[f'joint{joint}'].setValue(int(textbox_value))
        self.sliders[f'joint{joint}'].blockSignals(False)
        self.labels[f'joint{joint}'].setText(f'{textbox_value}°')
        self.publish_joint_states()

    def publish_joint_states(self):
        joint_state = JointState()
        joint_state.header = Header()
        joint_state.header.stamp = rospy.Time.now()
        joint_state.name = [f'joint{i}' for i in range(1, 7)]
        joint_state.position = [float(self.textboxes[f'joint{i}'].text()) * 3.14159 / 180.0 for i in range(1, 7)]
        self.joint_pub.publish(joint_state)

    def plan_and_execute(self):
        joint_goal = self.move_group.get_current_joint_values()
        for i in range(6):
            joint_goal[i] = float(self.textboxes[f'joint{i+1}'].text()) * 3.14159 / 180.0
        # Plan and execute
        self.move_group.go(joint_goal, wait=True)
        self.move_group.stop()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    gui = RobotGUI()
    gui.show()
    sys.exit(app.exec_())

