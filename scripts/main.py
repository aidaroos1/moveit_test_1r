#!/usr/bin/env python3
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QSlider, QLineEdit, QPushButton, QLabel
from PyQt5.QtCore import Qt
import rospy
from std_msgs.msg import Float64MultiArray

class RobotControlGUI(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.pub = rospy.Publisher('joint_commands', Float64MultiArray, queue_size=10)
        rospy.init_node('robot_control_gui', anonymous=True)

    def initUI(self):
        layout = QVBoxLayout()

        self.joint_sliders = []
        self.joint_texts = []

        for i in range(6):
            joint_layout = QHBoxLayout()

            label = QLabel(f'Joint {i+1}')
            joint_layout.addWidget(label)

            slider = QSlider(Qt.Horizontal)
            slider.setMinimum(-180)
            slider.setMaximum(180)
            slider.setValue(0)
            slider.valueChanged.connect(self.slider_changed)
            joint_layout.addWidget(slider)
            self.joint_sliders.append(slider)

            text = QLineEdit('0')
            text.setMaximumWidth(50)
            text.editingFinished.connect(self.text_changed)
            joint_layout.addWidget(text)
            self.joint_texts.append(text)

            layout.addLayout(joint_layout)

        self.execute_button = QPushButton('Execute')
        self.execute_button.clicked.connect(self.execute_command)
        layout.addWidget(self.execute_button)

        self.setLayout(layout)
        self.setWindowTitle('Robot Control GUI')
        self.show()

    def slider_changed(self):
        for i in range(6):
            self.joint_texts[i].setText(str(self.joint_sliders[i].value()))

    def text_changed(self):
        for i in range(6):
            try:
                value = int(self.joint_texts[i].text())
                self.joint_sliders[i].setValue(value)
            except ValueError:
                pass

    def execute_command(self):
        joint_values = [self.joint_sliders[i].value() for i in range(6)]
        msg = Float64MultiArray(data=joint_values)
        self.pub.publish(msg)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = RobotControlGUI()
    sys.exit(app.exec_())
