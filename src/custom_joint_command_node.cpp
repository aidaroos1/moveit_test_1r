#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
#include <moveit/move_group_interface/move_group_interface.h>

void jointCommandCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    static const std::string PLANNING_GROUP = "robot_arm";
    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);

    std::vector<double> joint_group_positions;
    move_group.getCurrentState()->copyJointGroupPositions(
        move_group.getCurrentState()->getRobotModel()->getJointModelGroup(PLANNING_GROUP), 
        joint_group_positions
    );

    for (size_t i = 0; i < msg->data.size(); ++i)
    {
        joint_group_positions[i] = msg->data[i] * M_PI / 180.0; // Convert to radians
    }

    move_group.setJointValueTarget(joint_group_positions);

    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    if (success)
    {
        move_group.move();
    }
    else
    {
        ROS_WARN("Planning failed");
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "custom_joint_command_node");
    ros::NodeHandle nh;

    ros::Subscriber sub = nh.subscribe("joint_commands", 10, jointCommandCallback);
    ros::spin();
    return 0;
}
